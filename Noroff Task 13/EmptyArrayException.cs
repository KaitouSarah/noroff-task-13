﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_13
{
    class EmptyArrayException : Exception
    {
        public EmptyArrayException() { }
        public EmptyArrayException(string message) : base(message) { }

        public EmptyArrayException(string message, Exception inner) : base(message, inner) { }
    }
}
