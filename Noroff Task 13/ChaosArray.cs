﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_13
{
    class ChaosArray<T> : EmptyArrayException
    {
        T[] array;

        //Standard constructor sets array size to 10 by default
        public ChaosArray()
        {
            array = new T[10];
        }
        public ChaosArray(int size)
        {
            array = new T[size];
        }

        //Adds a generic object to a random index of array
        public void Add(T chaosObject)
        {
            //Generate random number between 0 an length of array
            Random random = new Random();
            int index = random.Next(0, array.Length);

            //Add generic object to random index
            if (array[index] == null)
            {
                array[index] = chaosObject;
            } else if (isFull())
            {
                throw new FullArrayException("Could not place object as the array is full.");
            } else
            {
                throw new InvalidIndexException($"Could not place object at index {index} as it is occupied.");
            }
        }

        //Retrieve an object from random index in array as long as the array is not empty
        public T Retrieve()
        {
            if (notEmpty())
            {
                T chaosObject = default(T);

                while (chaosObject == null)
                {
                    //Generate random number between 0 an length of array
                    Random random = new Random();
                    int index = random.Next(0, array.Length);

                    chaosObject = array[index];
                }

                return chaosObject;
            } else
            {
                throw new EmptyArrayException("Cannot recieve object because the array is empty.");
            }
        }

        //Check that array is not empty
        public Boolean notEmpty()
        {
            foreach (T element in array)
            {
                if (element != null)
                {
                    return true;
                }
            }

            return false;
        }

        //Check if array is full
        public bool isFull()
        {
            foreach (T element in array)
            {
                if (element == null)
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}
