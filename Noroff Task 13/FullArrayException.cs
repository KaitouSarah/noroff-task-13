﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_13
{
    class FullArrayException : Exception
    {
        public FullArrayException() { }

        public FullArrayException(string message) : base(message) { }

        public FullArrayException(string message, Exception inner) : base(message, inner){ }
    }
}
