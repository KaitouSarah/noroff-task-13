﻿using System;

namespace Noroff_Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Two test arrays to make sure one of them is full for test part 3.
            ChaosArray<string> chaosArray1 = new ChaosArray<string>(2);
            ChaosArray<string> chaosArray2 = new ChaosArray<string>(1);

            //Three different test methods to test all exception cases.
            testRunPart1(chaosArray1);
            testRunPart2(chaosArray1);
            testRunPart3(chaosArray2);
        }

        private static void testRunPart1(ChaosArray<string> array)
        {
            //Try to retrieve object from empty array. Should throw exception
            try
            {
                Console.WriteLine($"Retrieved object was: {array.Retrieve()}\n~~~~~~~~~~~~~~~~~~~~~~");
            }
            catch (EmptyArrayException e)
            {
                Console.WriteLine(e.Message+"\n~~~~~~~~~~~~~~~~~~~~~~");
            }

            //Try to object to only free index in array. Should work
            try
            {
                array.Add("Twat");
                Console.WriteLine("Object Twat was added to the array.\n~~~~~~~~~~~~~~~~~~~~~~");
            }
            catch (InvalidIndexException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Would you like to try again, press 1. If not, press 0: ");
                int response = Int32.Parse(Console.ReadLine());

                switch (response)
                {
                    case 1:
                        testRunPart1(array);
                        break;
                    default:
                        break;
                }
            }

            //Try to retrieve object from array. Should work.
            try
            {
                Console.WriteLine($"Retrieved object was: {array.Retrieve()}.\n~~~~~~~~~~~~~~~~~~~~~~");
            }
            catch (EmptyArrayException e)
            {
                Console.WriteLine(e.Message + "\n~~~~~~~~~~~~~~~~~~~~~~");
            }
        }

        private static void testRunPart2(ChaosArray<string> array)
        {
            //Try to add object to array. 50/50 work or throw exception
            //Also lets the user try again if exception is thrown.
            try
            {
                array.Add("Dimwit");
                Console.WriteLine("Object Dimwit was added to the array.\n~~~~~~~~~~~~~~~~~~~~~~");
            }
            catch (InvalidIndexException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Would you like to try again, press 1. If not, press 0: ");
                int response = Int32.Parse(Console.ReadLine());
                Console.WriteLine("\n~~~~~~~~~~~~~~~~~~~~~~");

                switch (response)
                {
                    case 1:
                        testRunPart2(array);
                        break;
                    default:
                        break;
                }
            }
            catch (FullArrayException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void testRunPart3(ChaosArray<string> array)
        {
            //Try to add object to full array. Should throw exception and quit test part 3.
            try
            {
                array.Add("Turnip");
                Console.WriteLine("Object Turnip was added to the array.\n~~~~~~~~~~~~~~~~~~~~~~");
                array.Add("Wanker");
                Console.WriteLine("Object Wanker was added to the array.\n~~~~~~~~~~~~~~~~~~~~~~");
            }
            catch (InvalidIndexException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Would you like to try again, press 1. If not, press 0: ");
                int response = Int32.Parse(Console.ReadLine());
                Console.WriteLine("\n~~~~~~~~~~~~~~~~~~~~~~");

                switch (response)
                {
                    case 1:
                        testRunPart3(array);
                        break;
                    default:
                        break;
                }
            }
            catch (FullArrayException e)
            {
                Console.WriteLine(e.Message + "\n~~~~~~~~~~~~~~~~~~~~~~");
            }
        }
    }
}
