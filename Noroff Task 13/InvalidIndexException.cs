﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noroff_Task_13
{
    class InvalidIndexException : Exception
    {
        public InvalidIndexException() { }

        public InvalidIndexException(string message) : base(message) { }

        public InvalidIndexException(string message, Exception inner) : base(message, inner) { }
    }
}
