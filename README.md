# Noroff Task 13

Task 13: Custom Data Structure
� Create a custom collection - ChaosArray
� It should be able to store any type
� There should only be one way to insert an item in the collection �
which is inserted randomly
� There should only be one way to retrieve an item in the collection �
which returns a random item
� Use a custom Exception to handle instances where an item is being
inserted into an unavailable space and when an attempt to retrieve
an item that is not there

Note 1: I am using several custom exceptions
Note 2: Tests are just testing for string as type
Note 3: Should probably have made a seperate class for testing the methods and exceptions, but need time to work on 8 Queens problem.